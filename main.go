package main

import (
	"log"
	"net/http"
)

var (
	appName      = "random"
	appVersion   = "dev"
	appBuildTime = "0000-00-00 00:00:00"
)

func VersionHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte(appVersion))
}

func HealthHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte("OK"))
}

func main() {
	http.Handle("/version", http.HandlerFunc(VersionHandler))
	http.Handle("/health", http.HandlerFunc(HealthHandler))
	http.Handle("/"+appName+"/", InstumentAPIHandler(http.HandlerFunc(RandomHandler)))
	log.Println("Listening :8080 application port")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
