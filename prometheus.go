package main

import (
	"log"
	"net/http"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	inFlightGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Name: strings.Replace(appName, "-", "_", -1) + "_api_in_flight_requests",
		Help: "A gauge of requests currently being served by the wrapped handler.",
	})

	counter = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: strings.Replace(appName, "-", "_", -1) + "_api_requests_total",
			Help: "A counter for requests to the wrapped handler.",
		},
		[]string{"code", "method"},
	)

	// duration is partitioned by the HTTP method and handler. It uses custom
	// buckets based on the expected request duration.
	duration = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    strings.Replace(appName, "-", "_", -1) + "_api_request_duration_seconds",
			Help:    "A histogram of latencies for requests.",
			Buckets: []float64{.25, .5, 1, 2.5, 5, 10},
		},
		[]string{"handler", "method"},
	)

	// responseSize has no labels, making it a zero-dimensional
	// ObserverVec.
	responseSize = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    strings.Replace(appName, "-", "_", -1) + "_api_response_size_bytes",
			Help:    "A histogram of response sizes for requests.",
			Buckets: []float64{200, 500, 900, 1500},
		},
		[]string{},
	)
)

func init() {
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	go func() { log.Fatal(http.ListenAndServe(":9999", mux)) }()
	log.Println("Listening :9999 with Prometheus metrics")
}

func InstumentAPIHandler(handler http.Handler) http.Handler {
	return promhttp.InstrumentHandlerInFlight(inFlightGauge,
		promhttp.InstrumentHandlerDuration(duration.MustCurryWith(prometheus.Labels{"handler": "pull"}),
			promhttp.InstrumentHandlerCounter(counter,
				promhttp.InstrumentHandlerResponseSize(responseSize, handler),
			),
		),
	)
}
