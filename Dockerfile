FROM bitnami/minideb:latest
RUN install_packages ca-certificates

WORKDIR /root/
COPY ./app .
EXPOSE 8080
EXPOSE 9999
CMD ["./app"]