#!/usr/bin/env bash

echo ${DOCKER_PASS} | docker login -u${DOCKER_USER} --password-stdin https://${DOCKER_REPO_HOST}
docker build -t${DOCKER_REPO_HOST}/${DOCKER_USER}/${BITBUCKET_REPO_SLUG}:${VERSION} .
docker push ${DOCKER_REPO_HOST}/${DOCKER_USER}/${BITBUCKET_REPO_SLUG}:${VERSION}