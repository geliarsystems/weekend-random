#!/usr/bin/env bash

GO111MODULE=on go build -o app \
   -ldflags="-X main.appBuildTime=`date -u '+%F_%T'` -X main.appName=${BITBUCKET_REPO_SLUG} -X main.appVersion=${VERSION}" .