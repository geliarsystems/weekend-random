package main

import (
	"math/rand"
	"net/http"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: strings.Replace(appName, "-", "_", -1) + "_generated_random_numbers",
		Help: "The total number of generated numbers",
	})
)

func RandomHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write([]byte(strconv.Itoa(rand.Intn(100))))
	opsProcessed.Inc()
}
